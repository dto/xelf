;;; prototypes.lisp --- an alternative object system for Common Lisp

;; Copyright (C) 2007-2016  David O'Toole
;; Author: David O'Toole deeteeoh1138@gmail.com
;; Keywords: oop
;;
;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.

;;; Code: 

(in-package :xelf)

;;; Utility functions

(defun merge-hashes (a b &optional predicate)
  (prog1 a
    (maphash #'(lambda (key value)
		 (when (or (null predicate)
			   (funcall predicate key))
		   (setf (gethash key a) value)))
	     b)))

;;; Obsolete field reference syntax

(defvar *field-reference-prefix* "%")

(defun transform-tree (tester transformer tree)
  (cond ((consp tree)
	 ;; it's a cons. process the two subtrees.
	 (destructuring-bind (left . right) tree
	   (cons
	    ;; process left subtree.
	    (if (funcall tester left)
		(funcall transformer left)
		;; nothing to transform here. move on down the left side.
		(if (consp left)
		    (transform-tree tester transformer left)
		    left))
	    ;; process right subtree.
	    (transform-tree tester transformer right))))
	;; it's not a cons. test it.
	((funcall tester tree)
	 (funcall transformer tree))
	;; it failed the test. leave it alone.
	(t tree)))

;;; field references of the form %foo

(defun field-reference-p (form)
  "Return non-nil if FORM is a symbol like %foo."
  (if (symbolp form)
      (let ((name (symbol-name form)))
	(and (> (length name) 1)
	     (or (string= "%" (subseq name 0 1))
		 (string= "@" (subseq name 0 1)))
	     ;; don't catch double %%
	     (not (string= "%" (subseq name 1 2)))))))

(defmacro with-input-values (symbols form &body body)
  (assert (every #'symbolp symbols))
  (let ((thing (gensym)))
    (flet ((make-clause (symbol)
	     `(,symbol (evaluate 
			(input-block ,thing 
				     ,(make-keyword symbol))))))
      `(let* ((,thing ,form)
	      ,@(mapcar #'make-clause symbols))
	 ,@body))))
		      
(defun input-reference-p (form)
  "Return non-nil if FORM is a symbol like %%foo."
  (if (symbolp form)
      (let ((name (symbol-name form)))
	(and (> (length name) 2)
	     (string= "%%" (subseq name 0 2))))))

(defun make-accessor-macrolet-clause (symbol)
  (list symbol
	`(slot-value self
	  ',(make-non-keyword
	     ;; strip percent sign 
	    (subseq (symbol-name symbol) 1)))))

(defun make-accessor-flet-clause (symbol)
  `(,symbol (thing)
	    (field-value ,symbol thing)))

(defun transform-method-body (body)
  (let (fields)
    ;; collect %foo symbols used in body
    (transform-tree #'field-reference-p
		    #'(lambda (symbol)
			;; don't modify input
			(prog1 symbol
			  ;; just collect
			  (pushnew symbol fields)))
		    body)
    ;; arrange for the substitution
    `(symbol-macrolet 
	 ,(mapcar #'make-accessor-macrolet-clause fields)
       ,@body)))

(defmacro with-local-fields (&body body)
  (transform-method-body body))

(defmacro define-method
    (method-specifier prototype-name arglist &body method-body)
  ;; build the components of the defun
  (let ((method-name (etypecase method-specifier
		       (symbol method-specifier)
		       (list (first method-specifier))))
	(options (when (listp method-specifier)
		   (rest method-specifier))))
    (let* ((documentation (if (stringp (first method-body))
			      (first method-body)))
	   (body2 (remove-if #'stringp (transform-method-body method-body)))
	   ;; handle DECLARE forms when these appear first
	   (declaration (when (and (listp (first body2))
				   (eq 'declare (first (first body2))))
			  (first body2)))
	   (declaration2 (append '(declare (ignorable self))
				 (when declaration
				   ;; paste, skipping the declaration keyword
				   (rest declaration))))
	   (field-name (make-keyword method-name))
	   (method-symbol-name (symbol-name method-name)))
      `(defmethod ,(intern method-symbol-name) ((self ,(or prototype-name 'node)) ,@arglist)
	 ,body2))))

(defun transform-declaration (D)
  "Convert the declaration D into a canonical field
descriptor.

The descriptor D must be either a symbol, in which case a field is
defined with no options, or a list of the form:

 (:FIELD-NAME . OPTIONS)

Where OPTIONS is a property list of field options.

The returned entry will be of the form:

 (:FIELD-NAME OPTIONS) 

and will be suitable for use with the functions that operate on field
descriptors, and for inclusion in the association list
%field-descriptors.

See also `define-prototype'.
"
  (etypecase D
    (symbol (list (make-keyword D) nil))
    (list (list (make-keyword (car D)) (cdr D)))))

(defun plist-to-descriptors (plist)
  (let (descriptors)
    (loop while plist do
      (let* ((field (pop plist))
	     (value (pop plist)))
	(push (list field :initform value :initarg (make-keyword field) :accessor field)
	      descriptors)))
    (nreverse descriptors)))

;; (plist-to-descriptors '(:a 1 :b 2))
	
(defmacro define-prototype (name
			    (&key super 
				  documentation
				  &allow-other-keys)
			    &body declarations)
  (let* ((pre-descriptors (if (symbolp (first declarations))
			      (plist-to-descriptors declarations)
			      declarations))
	 (descriptors (mapcar #'transform-declaration 
			      pre-descriptors)))
    `(progn  
       (defclass ,name ,(when super (list super)) ,pre-descriptors))))
  
;;; Clipboard

;;; prototypes.lisp ends here
