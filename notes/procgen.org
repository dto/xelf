#+TITLE: Techniques for procedurally generated puzzles

* Flaws in previous approach
** Sometimes unsolvable levels
** Trivial/boring levels
** Levels wher eyou die really quickly
** Layout problems
** Missing parts

* Buffers and buffer operators

** (new 'buffer)
** singleton
** buffer operators accept zero or more buffers and return a buffer
** with-new-buffer
** translate
** resize
** combine
** arrange-below
** arrange-beside
** with-border

* Primitive puzzle operators

** wall-at
** wall-around-region
** wrap
** bricks
** horizontally
** vertically



 the flaws before were: occasionally unsovable levels, occasionally
      boring levels, layout problems, very difficult and touchy codeanyway the approach involves clearly separating invariants (properties
      you REQUIRE all maps to have), and flexible parts (anything that changes
      from map to map anyway. my new rule is this: after writing the function that generates a
      valid part of the level, the invariants should all be visible from the
      static structure of the source code, and the variant parts expressed by
      randomization of what the operators  i should have said "clearly separating invariants (expressed
      statically in the source code) from variations (expressed dynamically in
      the behavior of functions"
<dto> it probably amounts to level gen being context-free, so it may not be a
      completely universal approach, but i think i've benefited from applying
      it where possible. 

* Invariants

* Parameterizing generation and behavior
** with-difficulty
