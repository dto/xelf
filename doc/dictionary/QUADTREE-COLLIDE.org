#+OPTIONS: *:nil
** QUADTREE-COLLIDE (generic function)
 
: (OBJECT QUADTREE)
Detect and handle collisions of OBJECT with other objects within the
QUADTREE. The multimethod COLLIDE will be invoked on each pair of 
(OBJECT OTHER-OBJECT)
