#+OPTIONS: *:nil
** UPDATE-BOUNDING-BOX (generic function)
 
: (OBJECT QUADTREE)
Update the OBJECT's new bounding box and position in QUADTREE.
