#+OPTIONS: *:nil
** *NOMINAL-SCREEN-WIDTH* (variable)
Nominal width of the window,
in pixels.
