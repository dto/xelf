#+OPTIONS: *:nil
** TURN-LEFT (generic function)
 
: (NODE RADIANS)
Increase heading by RADIANS.
