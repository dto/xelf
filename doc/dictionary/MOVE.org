#+OPTIONS: *:nil
** MOVE (generic function)
 
: (NODE HEADING DISTANCE)
Move the NODE toward HEADING by DISTANCE units.
