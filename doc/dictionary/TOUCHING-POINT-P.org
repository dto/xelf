#+OPTIONS: *:nil
** TOUCHING-POINT-P (generic function)
 
: (NODE X Y)
Return non-nil if this NODE's bounding box touches
the point X,Y.
