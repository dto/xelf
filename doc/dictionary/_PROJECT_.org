#+OPTIONS: *:nil
** *PROJECT* (variable)
The name of the current project.
This is set by OPEN-PROJECT; use that instead.
