#+OPTIONS: *:nil
** COMPOSE (function)
 
: (BUFFER1 BUFFER2)
Return a new buffer containing all the objects from both BUFFER1
and BUFFER2. The original buffers are destroyed.
