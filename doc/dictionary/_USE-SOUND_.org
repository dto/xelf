#+OPTIONS: *:nil
** *USE-SOUND* (variable)
Non-nil (the default) is to use sound. Nil disables sound.
