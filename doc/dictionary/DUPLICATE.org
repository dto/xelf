#+OPTIONS: *:nil
** DUPLICATE (generic function)
 
: (NODE &REST INITARGS &KEY &ALLOW-OTHER-KEYS)
Make a copy of this NODE.
