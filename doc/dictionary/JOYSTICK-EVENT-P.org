#+OPTIONS: *:nil
** JOYSTICK-EVENT-P (function)
 
: (EVENT)
Return non-nil if the EVENT is a joystick event.
