#+OPTIONS: *:nil
** *WINDOW-POSITION* (variable)
Controls the position of the game window. Either a list of coordinates or the symbol :center.
