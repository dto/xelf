#+OPTIONS: *:nil
** *XELF-SERIES* (variable)
An integer giving the major API version of Xelf.
