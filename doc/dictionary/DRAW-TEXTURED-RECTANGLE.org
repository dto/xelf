#+OPTIONS: *:nil
** DRAW-TEXTURED-RECTANGLE (function)
 
: (X Y Z WIDTH HEIGHT TEXTURE &KEY (BLEND :ALPHA) (OPACITY 1.0)
     (VERTEX-COLOR "white"))
Draw an OpenGL textured rectangle at X, Y, Z with width WIDTH and height HEIGHT.
The argument TEXTURE is a string image name (or a texture returned by
FIND-TEXTURE). BLEND sets the blending mode and can be one
of :ALPHA, :ADDITIVE, :MULTIPLY.
