#+OPTIONS: *:nil
** *RESIZABLE* (variable)
When non-nil, game window will be resizable.
