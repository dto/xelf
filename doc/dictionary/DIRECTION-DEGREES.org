#+OPTIONS: *:nil
** DIRECTION-DEGREES (function)
 
: (DIRECTION)
Return the angle (in degrees) which DIRECTION points.
