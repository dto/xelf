#+OPTIONS: *:nil
** INDEX-RESOURCE (function)
 
: (RESOURCE)
Add the RESOURCE's record to the resource table.
If a record with that name already exists, it is replaced.
