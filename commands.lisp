;;; commands.lisp --- more operations

;; Copyright (C) 2006-2017  David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: games, gui

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Package declaration

(in-package :xelf)

;; Captions and labels

(defmethod set-caption-string ((self node) caption)
  (assert (stringp caption))
  (setf (slot-value self 'caption) caption))

(defmethod caption-string ((self node))
  (slot-value self 'caption))

(defmethod caption-width ((self node))
  (if (or (null (slot-value self 'caption)) (string= "" (slot-value self 'caption)))
      0
      (+ (dash 2)
	 (font-text-width (slot-value self 'caption) *node-font*))))

(defmethod draw-caption-string ((self node) string &optional color)
  (with-node-drawing 
      (with-slots (x y) self
	(let* ((dash *dash*)
	       (left (+ x (* 2 dash)))
	       (y0 (+ y dash 1)))
	  (draw-string string left y0 :color color)))))

(defmethod draw-label-string ((self node) string &optional color)
  (with-node-drawing 
      (with-slots (x y) self
        (let* ((dash *dash*)
               (left (+ x (* 2 dash)))
               (y0 (+ y dash 1)))
          (draw-string string left y0 :color color)))))

(defmethod draw-caption ((self node) expression)
  (draw-caption-string self (fancy-format-expression expression)))

;; Shell operations

(defmethod drag ((self shell) x y)
  (with-slots (target-x target-y) self
    (setf target-x (- x (window-origin-x)))
    (setf target-y (- y (window-origin-y)))
    (move-to self x y)))

(defmethod layout ((self shell))
  ;; (with-slots (target-x target-y) self
  ;;   (move-to self 
  ;; 	       (+ target-x (window-origin-x))
  ;; 	       (+ target-y (window-origin-y)))
  (with-slots (target-x target-y) self
    (setf target-x 0 target-y 0))
  (with-slots (inputs orientation) self
    (if (null inputs)
	(layout-as-null self)
	(ecase orientation
	  (:horizontal (layout-horizontally self))
	  (:vertical (layout-vertically self)))))
  (call-next-method)
  (move-to self (window-origin-x) (- (+ (window-origin-y) *screen-height*) (slot-value self 'height)))
  (resize self *screen-width* (slot-value self 'height)))

(defmethod layout :after ((self shell))
  (when *menubar* (layout *menubar*)))

(defmethod insert-output ((self shell) item)
  (unfreeze (input-node self :output))
  (accept (input-node self :output) item)
  (freeze (input-node self :output)))

(defmethod destroy-output ((self shell))
  (mapc #'destroy (%inputs (input-node self :output)))
  (setf (slot-value (input-node self :output) 'inputs) nil))

(defmethod replace-output ((self shell) items)
  (destroy-output self)
  (dolist (item items)
    (insert-output self item)))

(defmethod hit ((self menu) mouse-x mouse-y)
  (with-slots (x y expanded inputs width height) self
    (when (within-extents mouse-x mouse-y x y (+ x width) (+ y height))
      (flet ((try (item)
	       (hit item mouse-x mouse-y)))
	(if (not expanded)
	    self
	    (some #'try inputs))))))
;; ;; we're expanded. is the mouse to the left of this
;; ;; tree's header tab thingy?
;; (if (slot-value self 'top-level)
;; 	(when (and (< mouse-x (+ x (header-width self)))
;; 		   (< (header-height self) mouse-y))
;; 	  (some #'try inputs))
;; 	(or (some #'try inputs) self)))))))

(defmethod hit ((self shell) x y)
  (with-buffer self 
    (with-slots (inputs) self
      (labels ((try (b)
		 (when b 
		   (hit b x y))))
	(when inputs
	  (find-if #'try inputs))))))

;; (defmethod hit ((self shell) x y)
;;   (when (within-extents x y (slot-value self 'x) (slot-value self 'y) (+ (slot-value self 'x) (slot-value self 'width)) (+ (slot-value self 'y) (slot-value self 'height)))
;;     (flet ((try (thing)
;; 	       (hit thing x y)))
;; 	(or (some #'try (%inputs (slot-value self 'output)))
;; 	    self))))

(defmethod tap ((self shell) x y)
  (focus-on-entry self))

(defmethod alternate-tap ((self shell) x y) nil)

(defmethod get-prompt-label ((self shell)) (first (%inputs (input-node self :xcommand-area))))
(defmethod set-prompt-label ((self shell) label) (set-value (get-prompt-label self) label))
(defmethod get-prompt ((self shell)) (second (%inputs (input-node self :command-area))))
(defmethod set-prompt-line ((self shell) line) (set-value (get-prompt self) line))
(defmethod get-modeline ((self shell)) (input-node self :modeline))
(defmethod get-output ((self shell)) (input-node self :output))

(defmethod get-output-items ((self shell)) 
  (let ((phrase (first (%inputs (get-output self)))))
    (when phrase (%inputs phrase))))

(defmethod get-dialog ((self shell))
  (first (%inputs (get-output self))))

(defmethod get-argument-phrases ((self shell))
  (let ((container (second (get-output-items self))))
    (when (xelfp container) (%inputs container))))

(defmethod get-entries ((self shell))
  (cons (get-prompt self)
	(mapcar #'second (mapcar #'%inputs (get-argument-phrases self)))))

(defmethod evaluate-output ((self shell))
  (replace-output self (list (make-phrase (evaluate (get-dialog self))))))

(defmethod current-entry ((self shell))
  (let ((entries (get-entries self)))
    (with-slots (entry-index) self 
      (setf entry-index (mod entry-index (length entries)))
      (nth entry-index entries))))

(defmethod focus-on-entry ((self shell))
  (let ((entry (current-entry self)))
    (set-read-only entry nil)
    (grab-focus entry)
    (end-of-line entry)))

(defmethod next-entry ((self shell))
  (incf (slot-value self 'entry-index))
  (focus-on-entry self))

(defmethod previous-entry ((self shell))
  (decf (slot-value self 'entry-index))
  (focus-on-entry self))

(defmethod draw-background ((self shell) &key color)
  (with-slots (x y width height) self
    (draw-box x y (+ x width) (+ y height) :color "gray50" :alpha 0.8)))

(defmethod draw ((self shell))
  (with-style :flat
    (draw-background self))
  (mapc #'draw (slot-value self 'inputs))
  (when *notification*
    (draw *notification*)))

(defun shell () *shell*)
(defun shell-prompt () (get-prompt (shell)))
(defun shell-modeline () (get-modeline (shell)))
(defun shell-output () (get-output (shell)))
(defun shell-insert-output (object) (insert-output (shell) object))
(defun shell-destroy-output () (destroy-output (shell)))
(defun shell-evaluate-output ()  (evaluate-output (shell)))

;; Automatic layout


(defmethod update :after ((self shell))
  (layout self)
  (mapc #'layout (%inputs self))
  (mapc #'update (%inputs self))
  (when (and *menubar* (paused-p (current-buffer)))
    (layout *menubar*)
    (update *menubar*)))

;; Dialog box builder


(defclass dialog (phrase) 
  ((orientation :initform :vertical)
   (no-background :initform nil)
   (style :initform :rounded)))

(defmethod draw-background ((self dialog) &key color)
  (with-slots (x y width height) self
    (draw-patch self x y (+ x width) (+ y height) :color color :style :rounded)))

(defmethod tap ((dialog dialog) x y)
  (bring-to-front (or (parent dialog) dialog))
  (when (parent dialog) 
    (tap (parent dialog) x y)))

(defmethod freeze :after ((dialog dialog))
  (mapc #'freeze (inputs dialog)))

(defmacro define-dialog (name arglist &body body)
  `(progn
     (defun ,(action-name name) (&key ,@(mapcar #'car arglist)) ,@body)
     (export ',(action-name name))
     (defun ,(show-name name) (&rest args) 
       (show-dialog (apply #'make-instance ',(dialog-class-name name) args)
		    ,(command-name-string name)
		    :destroy-after-evaluate-p t))
     (export ',(show-name name))
     (define-visual-macro ,(dialog-class-name name)
	 (:super dialog
		 :slots ((orientation :initform :vertical)
		         (no-background :initform t))
		 :inputs ,(command-inputs name arglist)))
     (export ',(dialog-class-name name))
     (defmethod evaluate ((self ,(dialog-class-name name)))
       ;; call the command function
       (apply #'funcall #',(action-name name)
	      ;; with the evaluated results of
	      (mapcar #'evaluate 
		      ;; all the argument names/values
		      (mapcan #'identity 
			      (mapcar #'%inputs 
				      ;; from the dialog box
				      (%inputs (first (slot-value self 'inputs))))))))))

(defmacro define-command-dialog (name arglist &body body)
  `(define-dialog ,name ,arglist 
     (labels ((arg (name) (getf ^args^ name)))
       ,@body)))

(defclass property-sheet (dialog)
  ((orientation :initform :vertical)
   (initial-values :initform nil)
   (instance :initform nil :initarg :instance :accessor instance)
   (properties :initform nil :initarg :properties :accessor properties)))

(defmethod find-methods append ((dialog dialog))
  '(evaluate restore-initial-values cancel))

(defclass property-row (phrase)
  ((no-background :initform nil)
   (style :initform :rounded)))

(defmethod focus-on :after ((buffer buffer) (row property-row) &key (clear-selection t))
  (when row
    (focus-on buffer (second (inputs row)))))

(defclass property-value-entry (expression-entry) ())

(defmethod backtab ((entry property-value-entry))
  (backtab (current-buffer)))

(defmethod find-tab-parent ((entry property-value-entry))
  (parent (parent entry)))

(defmethod find-tab-proxy ((entry property-value-entry))
  (parent entry))

(defmethod evaluate ((entry property-value-entry))
  (get-value entry)) 

(defmethod draw-background ((self property-row) &key color)
  (with-slots (x y width height) self
    (draw-patch self x y (+ x width) (+ y height) :depressed nil :style :rounded)))

(defmethod initialize-instance :after ((sheet property-sheet) &key)
  (with-slots (inputs properties instance initial-values) sheet
    (dolist (property properties)
      (let ((row (make-sentence
		  (list
		   (make-instance 'pretty-symbol-entry
				  :value property
				  :locked t
				  :read-only t)
		   (make-instance 'property-value-entry
				  :value (slot-value instance property)
				  :read-only nil))
		  'property-row)))
	;; (setf (no-background row) nil)
	(push row inputs)
	(push (slot-value instance property) initial-values)))
    (setf initial-values (nreverse initial-values))
    (setf inputs (nreverse inputs))
    (update-parent-links sheet)
    (freeze sheet)))

(defmethod restore-initial-values ((sheet property-sheet))
  (with-slots (inputs properties instance initial-values) sheet
    (let ((i inputs)
	  (d initial-values))
      (flet ((entry () (second (inputs (first i)))))
	(dolist (property properties)
	  (set-value (entry) (pop d))
	  (pop i))))))

(defmethod get-property-object-pairs ((sheet property-sheet))
  (mapcar #'inputs (inputs sheet)))

(defmethod get-property-entries ((sheet property-sheet))
  (apply #'append (get-property-object-pairs sheet)))

(defmethod get-property-list ((sheet property-sheet))
  (mapcar #'evaluate (get-property-entries sheet)))

(defmethod apply-properties ((sheet property-sheet) &optional instance)
  (let ((plist (get-property-list sheet))
        (i (or instance (instance sheet))))
    (loop while plist do
	 (let* ((slot (pop plist))
		(value (pop plist)))
	   (setf (slot-value i slot) value)))))

(defmethod check-properties ((sheet property-sheet) &optional instance)
  (let ((plist (get-property-list sheet))
        (i (or instance (instance sheet))))
    (block checking
      (loop while plist do
	   (let* ((slot (pop plist))
		  (value (pop plist)))
	     (when (not (check-value-for-slot value i slot))
	       (notify (format nil "Error: Value ~S is of wrong type for slot ~S." value slot))
	       (return-from checking nil))))
      (return-from checking t))))

(defmethod cancel ((sheet property-sheet))
  (if (parent sheet)
      (destroy (parent sheet))
      (destroy sheet)))

(defmethod evaluate ((sheet property-sheet))
  (when (check-properties sheet)
    (apply-properties sheet (instance sheet))))

(defvar *instance* nil)

(defmacro define-properties-dialog (name slot-names &rest body)
  `(progn
     (defun ,(show-name name) (&optional (instance *instance*))
       (show-dialog (make-instance ',(dialog-class-name name) :instance instance)
		    ,(command-name-string name)
		    :destroy-after-evaluate-p nil))
     (defclass ,(dialog-class-name name) (property-sheet)
       ((properties :initform ',slot-names)))))

;; System object


(defvar *system* nil)

(defclass system (node)
  ((type :initform :system)
   (running :initform nil)))

(defmethod add-widget ((buffer buffer) (node node))
  (when (not (contains buffer node))
    (push node (inputs buffer))
    (adopt buffer node)))

(defmethod remove-widget ((buffer buffer) (node node))
  (delete-input buffer node))

(defun show-dialog (dialog title &key destroy-after-evaluate-p)
  (let ((frame (make-frame title dialog :destroy-after-evaluate-p destroy-after-evaluate-p)))
    (add-node (current-buffer) frame)
    (layout dialog)
    (layout frame)
    (center frame)
    (align-to-pixels frame)
    (freeze dialog)
    (close-menus *menubar*)
    nil))

(defun do-cut ()
  (cut))

(defun do-copy ()
  (copy))

(defun do-paste ()
  (paste))

(defun transport-play ()
  (play (current-buffer)))

(defun transport-pause ()
  (pause (current-buffer)))

(defun show-copyright-notice ()
  (let ((notice (make-instance 'text :text *copyright-notice*)))
    (add-node (current-buffer) notice 80 80)
    (resize-to-scroll notice 300 300)
    (setf (slot-value notice 'max-displayed-lines) 20)
    (layout notice)
    (center notice)
    (align-to-pixels notice)
    (bring-to-front notice)))

(defun save-before-exit ())

(defun create-project ())

;; (defun open-existing-project ((self system) (project-name string :default " "))

(defun save-changes ()
  (save-project))

(defun save-everything ()
  (save-project :force))

(defun create-trash ()
  (add-block (shell) (make-instance 'trash) 100 100))

(defun create-text ()
  (add-block (shell) (make-instance 'text) 100 100))

;; (defun create-listener ()
;;   (add-block (shell) (new listener) 100 100))

(defun ticks ()
  (get-ticks))

(defun exit-xelf* ()
  ;; TODO destroy textures
  (exit-xelf))

;; User dialogs


(define-dialog visit-buffer
    ((buffer-name (or (first *buffer-history*) 
  		      (buffer-name (current-buffer)))))
  (at-next-update (switch-to-buffer buffer-name)))

(define-dialog create-buffer
    ((buffer-name (uniquify-buffer-name "*untitled*"))
     (buffer-class (class-name (class-of (current-buffer)))))
  (at-next-update
   (switch-to-buffer (make-instance buffer-class :buffer-name buffer-name))))

(define-dialog paste-as-new-buffer
    ((buffer-name (uniquify-buffer-name "*pasted-buffer*"))
     (buffer-class (class-name (class-of (current-buffer))))
     (offset-x 0)
     (offset-y 0))
  (at-next-update
   (let ((buffer (make-instance buffer-class :buffer-name buffer-name)))
     (switch-to-buffer buffer-name)
     (paste (current-buffer) offset-x offset-y)
     (trim-conservatively (current-buffer)))))

(define-properties-dialog buffer-properties 
    (buffer-name width height z-sort-p background-image background-color 
		 window-scrolling-speed horizontal-scrolling-margin vertical-scrolling-margin))

(defmethod apply-properties :after ((dialog buffer-properties-dialog) &optional buffer)
  (notify (format nil "Applied buffer properties to ~S." buffer))
  (let ((buffer (or buffer (current-buffer))))
    (with-slots (height width buffer-name) buffer
      (resize buffer width height)
      (rename-buffer buffer buffer-name))))

(define-properties-dialog project-properties
    (path width height double-tap-time scale-output-to-window frame-rate resizable author author-contact title license))

(defmethod apply-properties :after ((dialog project-properties-dialog) &optional project)
  (with-slots (name path resizable scale-output-to-window frame-rate title author author-contact
		    width height double-tap-time license) project
    (setf *scale-output-to-window* scale-output-to-window)
    (set-frame-rate frame-rate)
    (setf *title* title)
    (setf *double-tap-time* double-tap-time)
    (setf *author* author)
    (setf *author-contact* author)
    (setf *screen-width* width)
    (setf *screen-height* height)
    (setf *resizable* resizable))
  (notify (format nil "Applied project properties to ~S." project)))

(defun all-buffer-names ()
  (loop for name being the hash-keys of *buffers* collect name))

(define-dialog change-buffer-class 
    ((new-class (class-name (class-of (current-buffer)))))
  (change-class (current-buffer) new-class))

;; Menu bar structure


(defparameter *project-menu*
  '(:label "Project"
    :inputs
    (;; (:label "Create a new project" :action create-project)
     ;; (:label "Open an existing project" :action open-existing-project)
     (:label "Save current changes" :action save-changes)
     (:label "Edit project properties" :action do-show-project-properties-dialog)
     (:label "Show current changes" :action show-changes)
     (:label "Show classes" :action show-classes-dialog)
     (:label "Export as archive" :action show-export-archive-dialog)
     (:label "Export as application" :action show-export-application-dialog)
     (:label "Publish to FTP" :action show-publish-ftp-dialog)
     (:label "Edit preferences" :action edit-preferences)
     (:label "Exit" :action show-exit-dialog))))

(defun do-show-project-properties-dialog ()
  (show-project-properties-dialog (current-project)))

(defparameter *edit-menu* 
  '(:label "Edit"
    :inputs
    ((:label "Cut" :action do-cut)
     (:label "Copy" :action do-copy)
     (:label "Paste" :action do-paste)
     (:label "Paste as new buffer" :action show-paste-as-new-buffer-dialog)
     (:label "Paste from" :action show-paste-from-dialog)
     (:label "Paste selection from" :action show-paste-selection-from-dialog)
     (:label "Select all" :action select-all)
     (:label "Clear selection" :action clear-selection)
     (:label "Invert selection" :action invert-selection)
     (:label "Node properties" :action show-node-properties-dialog)
     (:label "Shell command history" :action show-shell-history-dialog))))

(defparameter *play-menu*
  '(:label "Play"
    :inputs
    ((:label "Play" :action transport-play)
     (:label "Pause" :action transport-pause))))

(defparameter *buffers-menu*
  '(:label "Buffers"
    :inputs
    ((:label "Create a new buffer" :action show-create-buffer-dialog)
     (:label "Load a buffer from a file" :action show-load-buffer-from-file-dialog)
     (:label "Switch to buffer" :action show-switch-to-buffer-dialog)
     (:label "Edit buffer properties" :action do-show-buffer-properties-dialog)
     ;; (:label "Rename buffer" :action show-rename-buffer-dialog)
     ;; (:label "Resize buffer" :action show-resize-buffer-dialog)
     (:label "Trim empty space" :action do-trim)
     (:label "Trim empty space on right/bottom" :action do-trim-conservatively)
     (:label "Save buffer in project" :action show-save-buffer-in-project-dialog)
     (:label "Copy buffer" :action show-copy-buffer-dialog)
     (:label "Destroy buffer" :action show-destroy-buffer-dialog)
     (:label "Save buffer in new file" :action show-save-buffer-in-new-file-dialog)
     (:label "Change buffer class" :action show-change-buffer-class-dialog)
     (:label "Revert buffer" :action show-revert-buffer-dialog)
     (:label "Resize to background image" :action do-resize-to-background-image)
     (:label "Make snapshot" :action show-take-snapshot-dialog)
     (:label "View clipboard" :action view-clipboard)
     (:label "View buffer list" :action show-buffer-list))))

(defun show-buffer-list ()
  (at-next-update 
   (switch-to-buffer (find-buffer "*buffer-list*" :create t :class 'buffer-list))))

(defun do-resize-to-background-image ()
  (if (background-image (current-buffer))
      (resize-to-background-image (current-buffer))
      (notify "No background image to resize to.")))

(defun do-show-buffer-properties-dialog ()
  (show-buffer-properties-dialog (current-buffer)))

(defun do-trim () (trim (current-buffer)))
(defun do-trim-conservatively () (trim-conservatively (current-buffer)))

(defparameter *view-menu* 
  '(:label "View"
    :inputs
    ((:label "Move viewport to location" :action show-move-viewport-dialog)
     ;;(:label "Edit scrolling properties" :action show-scrolling-properties-dialog)
     (:label "Adjust zoom level" :action show-zoom-level-dialog)
     (:label "Reset zoom level" :action reset-zoom-level))))

(defparameter *resources-menu*
  '(:label "Resources"
    :inputs
    ((:label "Import new resource" :action show-import-resource-dialog)
     (:label "Edit resource properties" :action show-resource-properties-dialog)
     (:label "Edit resource in external program" :action edit-resource-externally)
     (:label "Search resources" :action show-search-resources-dialog)
     (:label "Export resources" :action show-export-resources-dialog)
     (:label "Browse resources" :action browse-resources)
     (:label "Create sprite sheet" :action show-create-sprite-sheet-dialog)
     (:label "Edit sprite sheet" :action show-edit-sprite-sheet-dialog)
     (:label "Edit fonts" :action show-edit-fonts-dialog)
     (:label "Preload resources" :action show-preload-resources-dialog)
     (:label "Clear cached resources" :action show-clear-cached-resources-dialog))))

(defparameter *tools-menu*
  '(:label "Tools" 
    :inputs
    ((:label "Create a Lisp listener" :action create-listener)
     (:label "Create a text box" :action create-text)
     (:label "Create a trash can" :action create-trash))))

(defparameter *desktop-menu*
  '(:label "Desktop" :inputs
    ((:label "Switch to Root Desktop" :action show-root-desktop)
     ;; (:label "Switch to Desktop 2" :action desktop-2)
     ;; (:label "Switch to Desktop 3" :action desktop-3)
     ;; (:label "Switch to Desktop 4" :action desktop-4)
     (:label "Arrange icons" :action auto-arrange-icons)
     (:label "Toggle snap-to-grid" :action toggle-snap-to-grid)
     (:label "Go back to the previous desktop" :action previous-desktop)
     (:label "Create a new desktop" :action create-desktop)
     (:label "Rename this desktop" :action rename-desktop)
     (:label "Delete this desktop" :action delete-desktop)
     (:label "Edit Desktop properties" :action show-desktop-properties-dialog))))

(defun find-root-desktop ()
  (find-buffer "*root-desktop*" :create t :class 'desktop))

(defun show-root-desktop ()
  (at-next-update 
   (switch-to-buffer (find-root-desktop))))

(defparameter *emacs-menu*
  '(:label "Emacs" :inputs
    ((:label "Show Emacs" :action switch-to-emacs)
     (:label "Edit Lisp" :action show-edit-lisp-dialog)
     (:label "Inspect object" :action show-inspect-object-dialog)
     (:label "Version control" :action show-version-control-dialog))))

(defparameter *windows-menu*
  '(:label "Windows"
    :inputs
    ((:label "Create a new window" :action create-window)
     (:label "Switch to the next window" :action next-window)
     (:label "Switch to window" :action switch-window)
     (:label "Close this window" :action close-window))))

(defparameter *devices-menu*
  '(:label "Devices"
    :inputs
    ((:label "Browse available devices" :action browse-devices)
     (:label "Scan for devices" :action scan-for-devices)
     (:label "Configure joystick" :action configure-joystick)
     (:label "Configure keyboard" :action configure-keyboard))))
;;(:label "Configure microphone" :action configure-microphone))))
;; (:label "Configure dance pad" :action configure-dance-pad))))

(defparameter *help-menu*
  '(:label "Help"
    :inputs
    ((:label "Copyright notice" :action show-copyright-notice)
     (:label "Documentation" :action show-documentation)
     (:label "General help" :action show-help)
     (:label "Examples" :action show-examples)
     (:label "API Reference" :action show-reference))))

(defun system-menu-entries ()
  (apply #'append
	 (mapcar #'list (list *project-menu*
			      *edit-menu*
			      *play-menu*
			      *buffers-menu*
			      *view-menu*
			      *resources-menu*
			      *desktop-menu*
			      *emacs-menu*
			      ;; *tools-menu*
			      ;; *windows-menu*
			      *devices-menu*
			      *help-menu*))))

(defparameter *system-menu* (system-menu-entries))

;; Traveling nodes

(defclass traveler (node)
  ((parent-buffer :initform nil)))

(defmethod add-node :before ((new-buffer buffer) (traveler traveler) &optional x y z)
  (with-slots (parent-buffer) traveler
    (when (and (not (null parent-buffer))
	       (xelfp parent-buffer)
	       (not (object-eq new-buffer parent-buffer)))
      (remove-node parent-buffer traveler)
      (setf parent-buffer new-buffer))))

;; Menubar class


(defclass menubar (tree traveler)
  ((category :initform :menu)
   (temporary :initform t)))

(defmethod make-halo ((self menubar)) nil)

(defmethod collide ((menu menu) (node node)) nil)
(defmethod collide ((node node) (menu menu)) nil)

(defmethod update :after ((self menubar))
  (layout self)
  (dolist (input (inputs self))
    (layout input)
    (mapc #'layout (inputs input))))

(defmethod initialize-instance :after ((self menubar) &key (menus *system-menu*))
  (with-slots (inputs) self
    (layout self)
    (setf inputs (make-menu menus))
    (dolist (each inputs)
      (setf (slot-value each 'top-level) t)
      (pin each))))

(defmethod hit ((self menubar) mouse-x mouse-y)
  (with-slots (x y width height inputs) self
    (when (within-extents mouse-x mouse-y x y (+ x width) (+ y height))
      ;; are any of the menus open?
      (let ((opened-menu (find-if #'expanded inputs)))
	(labels ((try (m)
		   (when m (hit m mouse-x mouse-y))))
	  (let ((moused-menu (find-if #'try inputs)))
	    (if (and ;; moused-menu opened-menu
		 (object-eq moused-menu opened-menu))
		;; we're over the opened menu, let's check if 
		;; the user has moused onto the other parts of the menubar
	        (flet ((try-other (menu)
			 (when (not (object-eq menu opened-menu))
			   (try menu))))
		  (let ((other (some #'try-other inputs)))
		    ;; are we touching one of the other menubar items?
		    (if (null other)
			;; nope, just hit the opened submenu items.
			(try opened-menu)
			;; yes, switch menus.
			(prog1 other
			  (unexpand opened-menu)
			  (expand other)))))
		;; we're somewhere else. just try the main menus in
		;; the menubar.
		(let ((candidate (find-if #'try inputs)))
		  (if (null candidate)
		      ;; the user moused away. close the menus.
		      self
		      ;; we hit one of the other menus.
		      (if opened-menu
			  ;; there already was a menu open.
			  ;; close this one and open the new one.
			  (prog1 candidate
			    (unexpand opened-menu)
			    (expand candidate))
			  ;; no menu was open---just hit the menu headers
			  (some #'try inputs)))))))))))

(defmethod draw-border ((self menubar) &optional ignore) nil)

(defmethod layout ((self menubar))
  (with-slots (x y width height inputs) self
    (setf x (window-origin-x) y (window-origin-y) width *screen-width* height (dash 1)) 
    (let ((x1 (dash 1)))
      (dolist (item inputs)
	(move-to item x1 y)
	(layout item)
	(incf x1 (dash 1 (header-width item)))
	(setf height (max height (slot-value item 'height)))))))

(defmethod draw ((self menubar))
  (with-slots (x y width inputs) self
    (let ((bar-height (dash 2 2 (font-height *font*))))
      (draw-box x y 
		width bar-height
		:color "gray18")
      (draw-line x bar-height width bar-height
		 :color "gray30")
      (with-slots (inputs) self
	(dolist (each inputs)
	  (draw each))))))

(defmethod draw :around ((self menubar))
  (when (shell-p (current-buffer))
    (call-next-method)))

(defmethod close-menus ((self menubar))
  (with-slots (inputs) self
    (when (some #'expanded inputs)
      (mapc #'unexpand inputs))))

(defmethod tap ((self menubar) x y)
  (let ((target (hit self x y)))
    (show-status (format nil "Hitting target ~S" target))
    (when (and (xelfp target)
               (not (object-eq target self)))
      (tap target x y))
    (close-menus self)))

;; Don't allow anything to be dropped on the menus, for now.

(defmethod draw-hover ((self menubar)) nil)

(defmethod accept ((self menubar) thing)
  (declare (ignore thing))
  nil)

;; Floating window frames


(define-handle frame-close-button :close
  :slots ((target-frame :initform nil :initarg :target-frame :accessor target-frame)))

(defmethod tap ((self frame-close-button) x y)
  (with-slots (parent) self
    (when parent (at-next-update (destroy (parent parent))))))

(defmethod layout ((self frame-close-button))
  (resize self 20 20))

(defclass titlebar-label (label) ())

(defmethod tap ((self titlebar-label) x y)
  (let ((it (parent (parent self))))
    (when it
      (tap it x y))))

(define-visual-macro titlebar
    (:super phrase
	    :slots ((frozen :initform t)
		    (orientation :initform :horizontal)
		    (no-background :initform t)	
		    (spacing :initform 2)
		    (dash :initform 1)
		    (category :initform :system))
	    :inputs (:close-button (make-instance 'frame-close-button)
				   :title (make-instance 'titlebar-label :font "sans-bold-11" :read-only t :locked t))))

(defmethod set-title ((self titlebar) title)
  (set-value (input-node self :title) title))

(define-visual-macro frame
    (:super phrase
	    :slots ((frozen :initform t)
		    (orientation :initform :vertical)
		    (no-background :initform t)
		    (spacing :initform 2)
		    (dash :initform 1)
		    (style :initform :rounded)
		    (destroy-after-evaluate-p 
		     :initform t 
		     :initarg :destroy-after-evaluate-p
		     :accessor destroy-after-evaluate-p)
		    (category :initform :system))
	    :inputs (:titlebar (make-instance 'titlebar)
			       :content (make-instance 'label :read-only t))))

(defmethod set-title ((self frame) title)
  (set-title (input-node self :titlebar) title))

(defmethod set-content ((self frame) content)
  (destroy (input-node self :content))
  (with-slots (inputs) self
    (setf (second inputs) content)))

(defun make-frame (title content &key destroy-after-evaluate-p (class 'frame))
  (let ((frame (make-instance class :destroy-after-evaluate-p destroy-after-evaluate-p)))
    (prog1 frame
      (set-title frame title)
      (set-content frame content)
      (center frame)
      (add-node (current-buffer) frame))))

(defmethod update :before ((self frame))
  (layout self))

(defmethod draw :before ((self frame))
  (multiple-value-bind (top left right bottom) (bounding-box self)
    (draw-patch self left top right bottom :color "gray30" :style :rounded)))

(defmethod destroy :before ((frame frame))
  (mapc #'destroy (inputs frame)))

(defmethod evaluate ((frame frame))
  (evaluate (second (inputs frame))))

(defmethod evaluate :after ((frame frame))
  (when (destroy-after-evaluate-p frame)
    (destroy frame)))

(defmethod make-halo :after ((frame frame))
  (bring-to-front (slot-value frame 'halo))
  (bring-to-front frame))

(defmethod context-menu ((frame frame))
  (context-menu (second (inputs frame))))

(defmethod show-context-menu ((node node))
  (let ((menu (context-menu node)))
    (when menu
      (add-node (current-buffer) menu)
      (move-to menu (window-pointer-x) (window-pointer-y))
      (bring-to-front menu))))

;; (defmethod as-drag ((self menu) x y)
;;   (make-menu-frame self))

;; Context menus


(defmethod make-method-menu-item ((self node) method target)
  (assert (and target method (symbolp method)))
  (let ((method-string (pretty-string method)))
    (list :label method-string
          :pinned t
	  :locked t
	  :method method
	  :target (find-object target)
	  :action (make-instance 'task :method-name method :target (find-object target)))))

(defclass context-menu (menu)
  ((no-background :initform nil)
   (category :initform :text)))

(defclass context-menu-item (menu)
  ((category :initform :text)))

(defmethod tap :around ((menu context-menu-item) x y)
  (call-next-method)
  (destroy (parent menu)))

(defmethod context-menu ((self node))
  (let ((methods (find-methods self)))
    (when methods
      (let (inputs)
	(dolist (method methods)
	  (push (make-method-menu-item self method self) inputs))
	(flet ((menu-item (args)
		 (make-menu args :target self :class 'context-menu-item)))
	  (make-instance 'context-menu
			 :inputs (mapcar #'menu-item (nreverse inputs))
			 :pinned nil
			 :expanded t
			 :locked t))))))

(defmethod initialize-instance :after ((menu context-menu) &key)
  (layout menu)
  (freeze menu))

(defmethod draw :before ((menu context-menu))
  (with-slots (x y width height) menu
    (draw-patch menu x y (+ width x) (+ y height) :style :rounded )))

(defmethod draw-highlight ((self context-menu))
  (with-slots (y height expanded action parent) self
    (when parent
      (with-slots (x width) parent
	(when (not expanded) 
	  (draw-box (+ x (dash 3))
		    (+ y (dash 1)) 
		    (- width (dash 4))
		    (+ height 1)
		    :color *highlight-background-color*)
	  (draw-label-string self (display-string self) *highlight-foreground-color*))))))

;; Show methods definitions in Emacs                                  :emacs:


(defmethod show-method ((self node) method)
  (let ((sym (definition method (find-object self))))
    (assert (symbolp sym))
    (let ((name (string-upcase 
  		 (format nil "~A::~A"
  			 (package-name (symbol-package sym))
  			 (symbol-name sym)))))
      (eval-in-emacs `(glass-show-definition ,name)))))

(defmethod show-definition ((self node))
  (let ((name 
	 (concatenate 'string 
		      (package-name *package*)
		      "::"
		      (prototype-variable-name 
		       (find-super-prototype-name self)))))
    (message "SHOWING DEF ON CL SIDE: ~S" name)
    (eval-in-emacs `(glass-show-definition ,name))))

;; Evaluate expressions in emacs

(defun eval-in-emacs (expression)
  (if (find-package :swank)
      (let ((sym (intern "EVAL-IN-EMACS" (find-package :swank))))
	(funcall sym expression))
      (message "(eval-in-emacs) failed; swank/emacs not available?")))

;; Switching between the Xelf and Emacs windows


(defmethod toggle-other-windows ((self buffer))
  (glass-toggle))

(defun glass-toggle ()
  (eval-in-emacs '(glass-toggle)))

(defun glass-show ()
  (eval-in-emacs '(glass-show)))

(defun glass-hide ()
  (eval-in-emacs '(glass-hide)))

(defun glass-show-at (x y)
  (eval-in-emacs 
   `(glass-show :x ,x :y ,y)))

;; Spreadsheets


(defun make-vector (n i)
  (make-array n :initial-element i :adjustable t))

(defun make-grid (rows cols)
  (let ((grid (make-vector rows nil)))
    (dotimes (row rows) 
      (setf (aref grid row) (make-vector cols nil))) 
    grid))

(defun grid-get (grid row col)
  (aref (aref grid row) col))

(defun grid-set (grid row col value)
  (let ((row (aref grid row)))
    (setf (aref row col) value)))

(defun grid-columns (grid)
  (length (aref grid 0)))

(defun grid-rows (grid)
  (length grid))

(defun vector-insert (oldvec pos elt)
  "Insert ELT into VECTOR at POS, moving elements at POS and
afterward down the list."
  (let* ((len (length oldvec))
	 (newvec (make-vector (+ len 1) nil)))
    (dotimes (i (+ 1 len))
      (setf (aref newvec i) (cond 
			      (( < i pos)
			       (aref oldvec i))
			      (( equal i pos)
			       elt)
			      (( > i pos) 
			       (aref oldvec (- i 1))))))
    newvec))

(defun vector-delete (oldvec pos)
  "Remove position POS from OLDVEC."
  (let* ((len (length oldvec))
	 (newvec (make-vector (- len 1) nil)))
    (dotimes (i (- len 1))
      (setf (aref newvec i) (cond
			      (( < i pos)
			       (aref oldvec i))
			      (( >= i pos)
			       (aref oldvec (+ i 1))))))
    newvec))

(defun grid-insert-row (grid row)
  "Returns a new grid with a row inserted at row ROW. You should
  replace the original grid with this one."
  (let* ((newrow (make-vector (grid-columns grid) nil)))
    (vector-insert grid row newrow)))

(defun grid-insert-column (grid col)
  "Returns a new grid with a column inserted at column COL. You
should replace the original grid with this one."
  (dotimes (i (grid-rows grid))
    (setf (aref grid i) (vector-insert (aref grid i) col nil)))
  grid)

(defun grid-delete-row (grid row)
  "Returns a new grid with the row ROW removed. You should replace the original 
grid with this one."
  (vector-delete grid row))

(defun grid-delete-column (grid col)
  "Returns a new grid with the column COL removed. You should
replace the original grid with this one."
  (dotimes (i (grid-rows grid))
    (setf (aref grid i) (vector-delete (aref grid i) col)))
  grid)

(defclass sheet (buffer) 
  ((top-margin :initform 18 :initarg :top-margin :accessor top-margin)
   (node-spacing :initform 10 :initarg :node-spacing :accessor node-spacing)
   (node-size :initform *default-icon-size* :initarg :node-size :accessor node-size)
   (snap-to-grid-p :initform t :initarg :snap-to-grid-p :accessor snap-to-grid-p)
   (x-offset :initform 0 :initarg :x-offset :accessor x-offset)
   (y-offset :initform 0 :initarg :x-offset :accessor x-offset)))

(defmethod top-margin ((sheet sheet))
  (if (shell-p sheet)
      (+ 8 (font-height *font*))
      0))

(defmethod find-methods append ((sheet sheet)) '(clean-up auto-arrange toggle-snap-to-grid))

(defmethod node-stride ((sheet sheet))
  (+ (node-size sheet)
     (node-spacing sheet)))

(defmethod populate ((sheet sheet)) nil)

(defmethod initialize-instance :after ((sheet sheet) &key)
  (resize sheet *screen-width* *screen-height*)
  (open-shell sheet)
  (populate sheet))

(defmethod grid-position ((sheet sheet) x y)
  (let ((stride (node-stride sheet))
	(spacing (node-spacing sheet))
	(top-margin (top-margin sheet)))
    (values (+ spacing (* x stride))
	    (+ top-margin spacing (* y stride)))))

(defmethod place-node ((sheet sheet) (node node) x y)
  (move-to node x y))

(defmethod auto-resize ((node node) (sheet sheet))
  (let ((size (node-size sheet)))
    (resize node size size)))

(defmethod last-column ((sheet sheet))
  (1- (length (aref (grid sheet) 0))))

(defmethod last-row ((sheet sheet))
  (1- (length (grid sheet))))

(defmethod last-screen-column ((sheet sheet))
  (1- (truncate (/ *screen-width* (node-stride sheet)))))

(defmethod last-screen-row ((sheet sheet))
  (1- (truncate (/ *screen-height* (node-stride sheet)))))

(defmethod snap-to-grid ((node node) (sheet sheet))
  (with-slots (x y) node
    (place-node sheet node
		(truncate (/ x (node-stride sheet)))
		(truncate (/ y (node-stride sheet))))))

(defmethod toggle-snap-to-grid ((sheet sheet))
  (setf (snap-to-grid-p sheet) (if (snap-to-grid-p sheet) nil t)))

;; Cell spreadsheets

(defclass cell-sheet (sheet)
  ((grid :initform (make-grid 20 8) :accessor grid :initarg :grid)
   (node-spacing :initform 2 :initarg :node-spacing :accessor node-spacing)
   (cursor-row :initform 0 :initarg :cursor-row :accessor cursor-row)
   (cursor-column :initform 0 :initarg :cursor-column :accessor cursor-column)
   (row-heights :initform nil :initarg :row-heights :accessor row-heights)
   (column-widths :initform nil :initarg :column-widths :accessor column-widths)
   (column-labels :initform nil :initarg :column-labels :accessor column-labels)
   (borders-p :initform nil :initarg :borders-p :accessor borders-p)
   (headers-p :initform t :initarg :headers-p :accessor headers-p)))

(defmethod populate ((sheet sheet))
  (dotimes (row (grid-rows (grid sheet)))
    (dotimes (column (grid-columns (grid sheet)))
      (setf (cell sheet row column)
            (make-instance 'property-value-entry :value 
			   (or (percent-of-time 50 (random 382712))
			       (random-choose '("a string" "a longer string" "(empty)"))))))))

(defmethod find-methods append ((sheet cell-sheet)) 
  '(toggle-headers toggle-borders toggle-snap-to-grid))

(defmethod toggle-headers ((sheet cell-sheet))
  (setf (headers-p sheet) (if (headers-p sheet) nil t)))

(defmethod set-cell ((sheet cell-sheet) (node node) row column)
  (add-node sheet node)
  (grid-set (grid sheet) row column node))

(defmethod get-cell ((sheet cell-sheet) row column)
  (grid-get (grid sheet) row column))

(defmethod cell ((sheet cell-sheet) row column)
  (get-cell sheet row column))

(defmethod (setf cell) ((node node) (sheet cell-sheet) row column)
  (set-cell sheet node row column))

(defmethod insert-row ((sheet cell-sheet))
  (with-slots (grid cursor-row) sheet
    (setf grid (grid-insert-column grid cursor-row))))

(defmethod insert-column ((sheet cell-sheet))
  (with-slots (grid cursor-column) sheet
    (setf grid (grid-insert-column grid cursor-column))))

(defmethod delete-row ((sheet cell-sheet))
  (with-slots (grid cursor-row) sheet
    (setf grid (grid-delete-column grid cursor-row))))

(defmethod delete-column ((sheet cell-sheet))
  (with-slots (grid cursor-column) sheet
    (setf grid (grid-delete-column grid cursor-column))))

(defmethod set-cursor ((sheet cell-sheet) row column)
  (setf (cursor-row sheet) row)
  (setf (cursor-column sheet) column))

(defmethod move-cursor ((sheet cell-sheet) direction)
  (with-slots (grid cursor-row cursor-column) sheet
    (let* ((rows (grid-rows grid))
	   (cols (grid-columns grid))
	   (cursor (list cursor-row cursor-column))
	   (new-cursor
	    (case direction
	      (:up (if (/= 0 cursor-row)
		       (list (- cursor-row 1) cursor-column)
		       cursor))
	      (:left (if (/= 0 cursor-column)
			 (list cursor-row (- cursor-column 1))
			 cursor))
	      (:down (if (< cursor-row (- rows 1))
			 (list (+ cursor-row 1) cursor-column)
			 cursor))
	      (:right (if (< cursor-column (- cols 1))
			  (list cursor-row (+ cursor-column 1))
			  cursor)))))
      (destructuring-bind (row column) new-cursor
	(set-cursor sheet row column)))))

(defmethod move-cursor-up ((sheet cell-sheet))
  (move-cursor sheet :up))

(defmethod move-cursor-left ((sheet cell-sheet))
  (move-cursor sheet :left))

(defmethod move-cursor-down ((sheet cell-sheet))
  (move-cursor sheet :down))

(defmethod move-cursor-right ((sheet cell-sheet))
  (move-cursor sheet :right))

(defmethod all-cells ((sheet cell-sheet))
  (let ((grid (grid sheet))
	(cell nil)
	(cells nil))
    (dotimes (r (grid-rows grid))
      (dotimes (c (grid-columns grid))
	(when (setf cell (grid-get grid r c))
	  (push cell cells))))
    (nreverse cells)))

(defparameter *sheet-header-font* "sans-9")

(defmethod header-width ((sheet cell-sheet))
  (if (headers-p sheet)
      (+ 12 (font-text-width (format nil "~d" (grid-rows (grid sheet))) *sheet-header-font*))
      0))

(defmethod header-height ((sheet cell-sheet))
  (if (headers-p sheet) 
      (font-height *sheet-header-font*)
      0))

(defparameter *minimum-column-width* 12)
(defparameter *minimum-row-height* 12)

(defmethod layout ((sheet cell-sheet))
  (with-slots (grid cursor-row node-spacing cursor-column column-widths row-heights headers-p borders-p) sheet
    (let* ((rows (grid-rows grid))
	   (columns (grid-columns grid))
	   (column-width 0)
	   (row-height 0)
	   (cell-width 0)
	   (cell nil)
	   (row-header-width 0)
	   (column-header-height 0)
	   (widths (make-vector columns 0))
	   (heights (make-vector rows 0))
	   (top-margin (top-margin sheet)))
      ;; compute row header width for along left side
      (setf row-header-width (header-width sheet))
      (setf column-header-height (header-height sheet))
      ;; compute widths of columns
      (dotimes (col columns)
	(setf column-width *minimum-column-width*)
	(dotimes (row rows)
	  (setf cell (cell sheet row col))
	  (when cell
	    (layout cell)
	    (setf column-width (max column-width (width cell)))))
	(setf (aref widths col) column-width))
      ;; compute heights of rows
      (dotimes (row rows)
	(setf row-height *minimum-row-height*)
	(dotimes (col columns)
	  (setf cell (cell sheet row col))
	  (when cell
	    (setf row-height (max row-height (height cell)))))
	(setf (aref heights row) row-height))
      ;; save layout info
      (setf row-heights heights)
      (setf column-widths widths)
      ;; move objects
      (dotimes (row rows)
        (dotimes (column columns)
	  (let ((cell (cell sheet row column)))
	    (when cell
	      (multiple-value-bind (top left right bottom)
		  (cell-bounding-box sheet row column)
		(move-to cell left top)
		(setf (fixed-width cell) (aref column-widths column))))))))))

(defmethod auto-arrange ((sheet cell-sheet))
  (layout sheet))	       

(defmethod cell-at ((sheet sheet) x y)
  (block finding
    (dotimes (row (grid-rows (grid sheet)))
      (dotimes (column (grid-columns (grid sheet)))
	(let ((cell (cell sheet row column)))
	  (when (hit cell x y)
	    (return-from finding
	      (values cell row column))))))
    (values nil nil nil)))

;; Rendering cell sheets


(defmethod draw-cell ((sheet cell-sheet) (node node))
  (draw node))

(defmethod cell-bounding-box ((sheet cell-sheet) row column)
  (let* ((top 
	  (+ (header-height sheet)
	     (top-margin sheet)
	     (reduce #'+ (subseq (row-heights sheet)
				 0 row))))
	 (left
	  (+ (header-width sheet)
	     (reduce #'+ (subseq (column-widths sheet)
				 0 column))))
	 (right (+ left (aref (column-widths sheet) column)))
	 (bottom (+ top (aref (row-heights sheet) row))))
    (values top left right bottom)))

(defparameter *empty-cell-color* "gray40")

(defmethod draw-empty-cell ((sheet cell-sheet) row column)
  (multiple-value-bind (top left right bottom)
      (cell-bounding-box sheet row column)
    (draw-box left top (- right left) (- bottom top)
	      :color *empty-cell-color*)))

(defmethod row-header-bounding-box ((sheet cell-sheet) row)
  (let* ((top 
	  (+ (header-height sheet)
	     (top-margin sheet)
	     (reduce #'+ (subseq (row-heights sheet)
				 0 row))))
	 (left 0)
	 (right (header-width sheet))
	 (bottom (+ top (aref (row-heights sheet) row))))
    (values top left right bottom)))

(defmethod column-header-bounding-box ((sheet cell-sheet) column)
  (let* ((top (top-margin sheet))
	 (left 
	  (+ (header-width sheet)
	     (reduce #'+ (subseq (column-widths sheet)
				 0 column))))
	 (right (+ left (aref (column-widths sheet) column)))
	 (bottom (+ top (header-height sheet))))
    (values top left right bottom)))

(defmethod draw-header-cell ((sheet cell-sheet) string top left right bottom)
  (draw-box left top (- right left) (- bottom top) :color "gray20")
  (draw-string string left top :color "white" :font *sheet-header-font*))

(defmethod row-label ((sheet cell-sheet) row)
  (format nil "~d" row))

(defmethod column-label ((sheet cell-sheet) column)
  (with-slots (column-labels) sheet
    (if (and (consp column-labels)
	     (> (length column-labels)
		column))
	(nth column column-labels)
	(format nil "~d" column))))

(defmethod draw-row-header ((sheet cell-sheet) row)
  (apply #'draw-header-cell
	 sheet
	 (row-label sheet row)
	 (multiple-value-list 
	  (row-header-bounding-box sheet row))))

(defmethod draw-column-header ((sheet cell-sheet) column)
  (apply #'draw-header-cell
	 sheet
	 (column-label sheet column)
	 (multiple-value-list 
	  (column-header-bounding-box sheet column))))

(defmethod draw ((sheet cell-sheet))
  (layout sheet)
  (draw-box 0 0 *screen-width* *screen-height* :color "gray30")
  (when (row-heights sheet)
    (dotimes (row (grid-rows (grid sheet)))
      (draw-row-header sheet row)
      (dotimes (column (grid-columns (grid sheet)))
	(draw-column-header sheet column)))
    (do-nodes (node sheet)
      (draw node))))

(defmethod process-tap :after ((cell-sheet cell-sheet) (node node) x y)
  (multiple-value-bind (cell row column) (cell-at cell-sheet x y)
    (when cell
      (glide-window-to-node cell-sheet node))))

;; Buffer list sheet


(defclass buffer-list (cell-sheet) 
  ((column-labels :initform '("Name" "Class" "Objects" "File"))))

(defmethod populate ((buffer-list buffer-list))
  (setf (grid buffer-list) (make-grid (hash-table-count *buffers*) 4))
  (let ((row 0))
    (dolist (buffer-name (all-buffer-names))  
      (let ((buffer (find-buffer buffer-name)))
	(setf (cell buffer-list row 0) (make-instance 'string-entry :value buffer-name :label buffer-name))
	(setf (cell buffer-list row 1) (make-instance 'property-value-entry :value (class-name (class-of buffer))))
	(setf (cell buffer-list row 2) (make-instance 'property-value-entry :value (hash-table-count (objects buffer))))
	(setf (cell buffer-list row 3) (make-instance 'property-value-entry :value (buffer-file-name buffer))))
      (incf row))))

(defmethod process-tap :after ((buffer-list buffer-list) (node node) x y)
  (multiple-value-bind (cell row column) (cell-at buffer-list x y)
    (when cell
      (let ((buffer (get-value (cell buffer-list row 0))))
	(at-next-update (switch-to-buffer buffer))))))

(defmethod visit :after ((buffer-list buffer-list))
  (with-slots (grid) buffer-list
    (setf grid nil)
    (do-nodes (node buffer-list)
      (destroy node))
    (populate buffer-list)))

(defclass image-preview (node) ())

(defmethod initialize-instance :after ((image-preview image-preview) &key image)
  (setf (slot-value image-preview 'image) image)
  (resize image-preview 64 64))

(defmethod draw ((image-preview image-preview))
  (with-slots (image x y width height) image-preview
    (draw-image image x y :width width :height height)))

(defclass image-list (cell-sheet) 
  ((column-labels :initform '("Preview" "Width" "Height" "File"))))

(defmethod populate ((image-list image-list))
  (let ((images (all-image-names (current-project))))
    (setf (grid image-list) (make-grid (length images) 4))
    (let ((row 0))
      (dolist (image-name images)
	(setf (cell image-list row 0) (make-instance 'image-preview :image image-name))
	(setf (cell image-list row 1) (make-instance 'property-value-entry :value (image-width image-name)))
	(setf (cell image-list row 2) (make-instance 'property-value-entry :value (image-height image-name)))
	(setf (cell image-list row 3) (make-instance 'property-value-entry :value (find-file (current-project) image-name)))
	(incf row)))))

;; (defmethod process-tap :after ((image-list image-list) (node node) x y)
;;   (multiple-value-bind (cell row column) (cell-at image-list x y)
;;     (when cell
;;       (let ((buffer (get-value (cell image-list row 0))))
;; 	(at-next-update (switch-to-buffer buffer))))))

(defmethod visit :after ((image-list image-list))
  (with-slots (grid) image-list
    (setf grid nil)
    (do-nodes (node image-list)
      (destroy node))
    (populate image-list)))

;; Desktop

;; The included icons are inspired by the Xerox Star interface.
;; Ironically, when using SBCL these objects are also implemented via
;; PCL, the CLOS implementation written originally by Xerox.


(defparameter *default-icon-size* 64)

(defvar *icon-size* *default-icon-size*)

(defparameter *icon-font* "sans-9")

(defparameter *default-icon-color* "white")

(defparameter *icon-images*
  '(:empty "icon-empty.png"
    :caption "icon-caption.png"
    :document "icon-document.png"
    :folder "icon-folder.png"
    :grid "icon-grid.png"
    :open-folder "icon-open-folder.png"
    :project "icon-project.png"))

(defun icon-image (name)
  (getf *icon-images* name))

(defclass icon (node)
  ((image :initform (icon-image :empty) :initarg :image :accessor image)
   (color :initform *default-icon-color* :initarg :color :accessor color)
   (left-margin :initform 5 :initarg :left-margin :accessor left-margin)
   (last-tap-time :initform nil)
   (right-margin :initform 5 :initarg :right-margin :accessor right-margin)
   (bottom-margin :initform 15 :initarg :bottom-margin :accessor bottom-margin)
   (caption :initform nil :initarg :caption :accessor caption)
   (action :initform nil :initarg :action :accessor action)))

(defparameter *double-tap-time* 8)

(defmethod tap ((self icon) x y)
  (with-slots (last-tap-time) self
    (let* ((time *updates*)
	   (elapsed-time (- time (or last-tap-time 0))))
      (cond ((null last-tap-time)
	     (setf last-tap-time time))
	    ((<= elapsed-time *double-tap-time*)
	     (setf last-tap-time nil)
	     (double-tap self x y))))))

(defmethod update :before ((self icon))
  (with-slots (last-tap-time) self
    ;; we actually catch the end of single-click here.
    (when (and last-tap-time
	       (> (- *updates* last-tap-time)
		  *double-tap-time*))
      (setf last-tap-time nil)
      (select self))))

(defmethod double-tap ((self icon) x y)
  (evaluate self))

(defmethod initialize-instance :after ((icon icon) &key caption action)
  (resize icon *icon-size* *icon-size*))

(defmethod draw ((icon icon))
  (with-slots (x y width height caption color left-margin right-margin bottom-margin image) icon
    (set-vertex-color color)
    (draw-image image x y :width width :height height)
    (set-vertex-color "white")
    (when caption
      (draw-string caption (+ x left-margin) (- (+ y height) bottom-margin) :color "black" :font *icon-font*))))

(defparameter *default-desktop-background-color* "gray30")

(defclass desktop (sheet)
  ((background-color :initform *default-desktop-background-color*)
   (top-margin :initform 18 :initarg :top-margin :accessor top-margin)))

(defmethod find-methods append ((desktop desktop)) '(clean-up auto-arrange))

(defmethod clean-up ((desktop desktop))
  (dolist (node (find-instances desktop 'node))
    (snap-to-grid node desktop)
    (auto-resize node desktop)))

(defmethod auto-arrange-column ((desktop desktop) nodes column)
  (let ((row (last-screen-row desktop)))
    (dolist (node nodes)
      (multiple-value-bind (x y) (grid-position desktop column row)
	(place-node desktop node x y)
	(decf row)))))

(defmethod auto-arrange ((desktop desktop))
  (let* ((nodes-per-column (last-screen-row desktop))
	 (last-screen-column (last-screen-column desktop))
	 (column last-screen-column)
	 (nodes (find-instances desktop 'node)))
    (loop while (and nodes
		     (not (minusp column)))
       do (progn (if (> (length nodes) nodes-per-column)
		     (progn
		       (auto-arrange-column desktop (subseq nodes 0 (1- nodes-per-column)) column)
		       (setf nodes (subseq nodes nodes-per-column)))
		     (auto-arrange-column desktop nodes column))
		 (decf column)))))

(defun strip-asterisks (string)
  (remove #\* string))

(defclass folder-icon (icon)
  ((image :initform (icon-image :folder))
   (bottom-margin :initform 18 :initarg :bottom-margin :accessor bottom-margin)
   (buffer-name :initform nil :initarg :buffer-name :accessor buffer-name)))

(defmethod initialize-instance :after ((icon folder-icon) &key buffer-name)
  (setf (caption icon) (strip-asterisks buffer-name)))

(defmethod evaluate ((icon folder-icon))
  (at-next-update (switch-to-buffer
		   (find-buffer (buffer-name icon)
				:create t
				:class 'desktop))))

(defclass buffer-icon (icon)
  ((image :initform (icon-image :grid))
   (buffer-name :initform nil :initarg :buffer-name :accessor buffer-name)))

(defmethod initialize-instance :after ((icon buffer-icon) &key buffer-name)
  (setf (caption icon) buffer-name))

(defmethod evaluate ((icon buffer-icon))
  (at-next-update (switch-to-buffer
		   (find-buffer (buffer-name icon)
				:create t
				:class 'buffer))))

(defclass button-icon (icon)
  ((image :initform (icon-image :caption))
   (action :initform nil :initarg :action :accessor action)))

(defmethod initialize-instance :after ((icon button-icon) &key action)
  (when (null (caption icon))
    (setf (caption icon) (format nil "~S" action))))

(defmethod evaluate ((icon button-icon))
  (funcall (action icon)))

(defclass text-icon (icon)
  ((image :initform (icon-image :document))
   (text :initform nil :initarg :text :accessor text)
   (left-margin :initform 14 :initarg :left-margin :accessor left-margin)
   (right-margin :initform 5 :initarg :right-margin :accessor right-margin)
   (bottom-margin :initform 17 :initarg :bottom-margin :accessor bottom-margin)
   (title :initform nil :initarg :title :accessor title)))

(defmethod initialize-instance :after ((icon text-icon) &key buffer-name title)
  (setf (caption icon) title))

(defmethod evaluate ((icon text-icon))
  (let ((text (make-instance 'text :text (text icon))))
    (add-node (current-buffer) text)
    (center text)
    (bring-to-front text)
    (align-to-pixels text)))

(defmethod default-icons ((desktop desktop))
  (list
   (make-instance 'text-icon :text *help-text* :title "Help")
   (make-instance 'text-icon :text *copyright-notice* :title "License")
   (make-instance 'button-icon
		  :image (icon-image :open-folder) 
		  :caption "Cell sheet"
		  :action #'(lambda ()
			      (at-next-update
			       (switch-to-buffer
				(make-instance 'cell-sheet :buffer-name "*cell sheet*")))))
   ;; (make-instance 'buffer-icon :buffer-name "New Buffer")
   ;; (make-instance 'buffer-icon :buffer-name "New Class")
   ;; (make-instance 'folder-icon :buffer-name "Classes")
   ;; (make-instance 'folder-icon :buffer-name "Resources")
   ;; (make-instance 'folder-icon :buffer-name "Buffers")
   (make-instance 'button-icon :image (icon-image :project) 
		  :action #'show-buffer-list
		  :caption "Buffer List")
   (make-instance 'button-icon :image (icon-image :project) 
		  :action #'show-image-list
		  :caption "Image List")))

(defun show-image-list ()
  (at-next-update (switch-to-buffer (find-buffer "*image-list*" :create t :class 'image-list))))

(defmethod populate ((desktop desktop))
  (dolist (icon (default-icons desktop))
    (add-node desktop icon))
  (auto-arrange desktop))

(defmethod resize :after ((desktop desktop) width height)
  (auto-arrange desktop))

(defclass buffer-frame (frame)
  ((buffer :initarg :buffer :accessor buffer)))

(defmethod initialize-instance :after ((frame buffer-frame) &key buffer)
  (assert (not (null buffer)))
  (set-title frame (buffer-name buffer))
  (set-content frame buffer)
  (resize frame 320 200))

(defmethod layout ((frame buffer-frame)) nil)

(defmethod resize :after ((frame buffer-frame) width height)
  (with-slots (x y buffer) frame
    (clip buffer x y width height)))

(defun show-cell-sheet ()
  (let ((frame (make-instance 'buffer-frame :buffer (make-instance 'cell-sheet))))
    (add-node (current-buffer) frame)
    (resize frame 400 300)
    (center frame)
    (align-to-pixels frame)
    (bring-to-front frame)))

(defun show-cell-sheet* ()
  (let ((sheet (find-buffer "*cell sheet*" :create t :class 'cell-sheet)))
    (add-node (current-buffer) sheet)
    ;; (resize sheet 400 400)
    (move-to sheet 200 200)))
