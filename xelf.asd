;;; -*- Mode: Lisp; -*-

(defpackage :xelf-asd)

(in-package :xelf-asd)

(asdf:defsystem xelf
  :name "xelf"
  :version "5.0"
  :maintainer "David T O'Toole <dto@xelf.me>"
  :author "David T O'Toole <dto@xelf.me>"
  :license "GNU Lesser General Public License (LGPL) Version 3"
  :description "Xelf is a simple 2D game engine for Common Lisp"
  :serial t
  :depends-on (:lispbuilder-sdl 
	       :lispbuilder-sdl-image 
	       :lispbuilder-sdl-ttf
	       :lispbuilder-sdl-mixer
	       ;; :lispbuilder-sdl-gfx
	       :uuid
	       :babel
	       :salza2
	       :chipz
	       :usocket
	       :cl-fad
               :cl-ppcre
	       :cl-opengl)
  :components ((:file "package")
               (:file "quickprop")
	       (:file "generics" :depends-on ("package"))
	       (:file "rgb" :depends-on ("generics" "package"))
	       (:file "keys" :depends-on ("generics" "package"))
	       (:file "prototypes" :depends-on ("generics" "package"))
	       (:file "xelf" :depends-on ("keys" "prototypes" "rgb" "generics" "package"))
	       (:file "gui" :depends-on ("xelf"))
	       (:file "shell" :depends-on ("gui"))
	       (:file "commands" :depends-on ("shell"))
	       (:file "upnp" :depends-on ("commands"))
	       (:file "netplay" :depends-on ("upnp"))))


