;;; build-it.lisp --- script for making linux/windows SBCL binaries
;;; (C) Copyright 2011-2016 by David O'Toole <dto@xelf.me>

;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Linux usage at shell prompt:
;;
;;    cd ~/myproject
;;    sbcl --load build-game.lisp --name myproject

;; By default this attempts to load a system called MYPROJECT and to
;; have the binary called MYPROJECT.BIN start up by calling
;; (MYPROJECT::MYPROJECT). To change the binary filename, system name,
;; or MAIN function, give the --binary, --system, or --main arguments
;; to the script as needed.

;; If you have set up Wine properly, you can just add "wine" before
;; sbcl in the command line above to magically get a Win32 EXE file
;; that works on Real Windows(TM) but using only Free Software.

;; To set up Linux cross-compiling for Win32, install Wine first
;; through your distribution's package manager. Then download and
;; install the latest Win32 build of SBCL from
;; http://sbcl.org/platform-table.html and use Wine to run the
;; graphical installer. Default options are fine. This will install to
;; ~/.wine/drive_c/ area.

;; Now download the installer "quicklisp.lisp" from
;; https://www.quicklisp.org/beta/ 

;; Then in the Linux shell, run the command "wine sbcl quicklisp.lisp"
;; which will actually drop you into the Windows SBCL.EXE and create a
;; parallel installation of Quicklisp into
;; ~/.wine/drive_c/users/.../quicklisp

;; Make sure to use (ql:add-to-init-file) as instructed by the
;; Quicklisp installer.

;; Now you'll need a second symlink to your project, this time so that
;; the Win32 Quicklisp can find it, in:
;;
;;    ~/.wine/drive_c/users/USERNAME/quicklisp/local-systems/
;; 


#-quicklisp (load "z:/home/dto/quicklisp/setup.lisp")
(push (merge-pathnames "lib/" *default-pathname-defaults*) asdf:*central-registry*)
(require 'sb-posix)
(setf sb-impl::*default-external-format* :utf-8)

(defun argument (name)
  (let* ((args sb-ext:*posix-argv*)
	 (index (position name args :test 'equal))
	 (value (when (and (numberp index)
			   (< index (length args)))
		  (nth (1+ index) args))))
    value))

(defparameter *name* (argument "--name"))
(defparameter *binary*
  (or (argument "--binary")
      #+win32 (concatenate 'string *name* ".exe")
      #+linux (concatenate 'string *name* ".bin")))
(defparameter *system*
  (or (argument "--system")
      (intern *name* :keyword)))
(ql:quickload *system*)
(defparameter *main*
  (or (argument "--main")
      (concatenate 'string (string-upcase *name*) "::" (string-upcase *name*))))

(sb-ext:save-lisp-and-die *binary*
			  :toplevel (lambda ()
                                      (setf xelf::*executable* 
				      (sb-posix:putenv
				       (format nil "SBCL_HOME=~A" 
				      	       #.(sb-ext:posix-getenv "SBCL_HOME")))
				      (funcall (read-from-string *main*))
				      0)
			  :executable t)

