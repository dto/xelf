* Task list
** TODO close menus after buffer switch
** TODO fix weird buffer switches when background click
** TODO cell cursor movement 
** TODO buffer scrolling with mouse wheel 
** TODO context tool buttons on / above modeline
** TODO [#A] allow recursive buffer view and split views
**** TODO 
**** TODO fix project-window always before draw
**** TODO separate draw and draw-primary 
** TODO [#B] Design user experience
*** TODO launching external programs to edit assets. gimp, audacity, switch-to-emacs,. etc
*** TODO folders are virtual views, they don't nest
** TODO [#A] command undo/redo
** TODO [#A] design layered tile map editing
** TODO [#A] snap-to-grid and offset
** TODO [#A] modeline and mousewheel controls for scrolling the buffer window
** TODO [#A] toggle-snap-to-grid
** TODO save/load functionality
*** TODO [#A] test serialization
*** TODO [#A] show-save-buffer-in-project-dialog
*** TODO [#A] save-changes
*** TODO [#A] loading projects
** TODO [#B] button class
** TODO [#B] checkbox
** TODO [#B] recover sidebar
** TODO [#B] fix can't drag item out of shell
** TODO [#B] show-classes-dialog
** TODO [#B] show-paste-from-dialog
** TODO [#B] show-paste-selection-from-dialog
** TODO [#B] show-node-properties-dialog
** TODO [#B] show-load-buffer-from-file-dialog
** TODO [#B] show-copy-buffer-dialog
** TODO [#B] show-destroy-buffer-dialog
** TODO [#B] show-move-viewport-dialog
** TODO [#B] show-import-resource-dialog
** TODO [#B] show-resource-properties-dialog
** TODO [#B] show-documentation
** TODO [#B] hand me a tile / reference
** TODO [#B] test multiline text edit
** TODO [#B] choose and export more accessor names for buffer/node slots
** TODO [#B] Document how to clear all caches
** TODO [#B] should block widgets be marked as :collision-type nil?
** TODO [#B] System class for session management and device driver
** TODO [#B] Resource class
*** TODO RELOAD
*** TODO PROPERTIES   
*** TODO PROPERTY
*** TODO SETF PROPERTY
** TODO [#B] Emacs class 
*** TODO EVAL-IN-EMACS
*** TODO SHOW-DEFINITION
*** TODO SHOW-EMACS
*** TODO SHOW-XELF
*** TODO TOGGLE-EMACS
** TODO [#B] Database class for serializing objects / variables
*** TODO WRITE-DATA
*** TODO READ-DATA
** TODO [#B] Gamepad class
** TODO [#B] Keyboard class
** TODO [#B] Properties class
** TODO [#B] Factory class 
** TODO [#B] Network class
** TODO [#B] Viewport class (buffer window)
** TODO [#C] themeable "index card pastel" vertex colors for folders
** TODO [#C] view-clipboard
** TODO [#C] configurable double click time
** TODO [#C] add more notifications for cut/copy etc
*** TODO Show yellow notification string on modeline
** TODO [#C] show-exit-dialog
** TODO [#C] show-changes
** TODO [#C] show-export-archive-dialog
** TODO [#C] show-export-application-dialog
** TODO [#C] show-publish-ftp-dialog
** TODO [#C] edit-preferences
** TODO [#C] show-shell-history-dialog
** TODO [#C] show-save-buffer-in-new-file-dialog
** TODO [#C] show-revert-buffer-dialog
** TODO [#C] resize-to-background-image
** TODO [#C] show-take-snapshot-dialog
** TODO [#C] show-zoom-level-dialog
** TODO [#C] reset-zoom-level
** TODO [#C] edit-resource-externally
** TODO [#C] show-search-resources-dialog
** TODO [#C] show-export-resources-dialog
** TODO [#C] browse-resources
** TODO [#C] show-create-sprite-sheet-dialog
** TODO [#C] show-edit-sprite-sheet-dialog
** TODO [#C] show-edit-fonts-dialog
** TODO [#C] show-preload-resources-dialog
** TODO [#C] show-clear-cached-resources-dialog
** TODO [#C] switch-to-desktop
** TODO [#C] auto-arrange-icons
** TODO [#C] previous-desktop
** TODO [#C] create-desktop
** TODO [#C] rename-desktop
** TODO [#C] delete-desktop
** TODO [#C] show-desktop-properties-dialog
** TODO [#C] switch-to-emacs
** TODO [#C] show-edit-lisp-dialog
** TODO [#C] show-inspect-object-dialog
** TODO [#C] show-version-control-dialog
** TODO [#C] browse-devices
** TODO [#C] scan-for-devices
** TODO [#C] configure-joystick
** TODO [#C] configure-keyboard
** TODO [#C] show-examples
** TODO [#C] show-reference
** TODO [#C] dropdown list
** TODO [#C] export-as-application
** TODO [#C] make each buffer have its own command-history?
** TODO [#C] change :no-background to :inhibit-background-p
** TODO [#C] radio buttons
** TODO [#C] pinnable pop up menus
** TODO [#C] allow tear-off menus
** TODO [#C] object/tool palette
** TODO [#C] mimic eldoc mode on hintline
** TODO [#C] test sexp correspondence
** TODO [#C] phrase creation
** TODO [#C] phrase fillout
** TODO [#C] Automate doc build
** TODO [#C] fix shutdown crash in TAP ENTRY
** TODO [#C] fix company-quickhelp not working in org (check portacle's config)
** TODO [#C] Fix PATH not being a NODE?
* Archived Entries

** DONE fix MOVE/RESIZE handles disappearing after resize
   CLOSED: [2017-04-09 Sun 15:39]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-09 Sun 16:45
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE test selected-object
   CLOSED: [2017-04-09 Sun 15:43]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-09 Sun 16:45
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE (setf *var* (selected-object))
   CLOSED: [2017-04-09 Sun 15:43]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-09 Sun 16:45
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE select should single-select unless CONTROL is held
   CLOSED: [2017-04-09 Sun 15:50]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-09 Sun 16:45
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] destroy-selection
   CLOSED: [2017-04-09 Sun 16:06]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-09 Sun 16:45
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] region drawing
   CLOSED: [2017-04-09 Sun 16:45]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-09 Sun 16:45
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix need for AT-NEXT-UPDATE in using SWITCH-TO-BUFFER from command line
   CLOSED: [2017-04-10 Mon 05:10]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 05:10
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE clipboard buffer should be titled *clipboard*
   CLOSED: [2017-04-10 Mon 11:35]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 11:36
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE test add-buffer
   CLOSED: [2017-04-10 Mon 05:10]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 11:36
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE Fix incorrect mouse coords after enlarging window
   CLOSED: [2017-04-10 Mon 12:03]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 13:28
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE add status line
   CLOSED: [2017-04-10 Mon 11:58]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 13:28
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix clear-selection
   CLOSED: [2017-04-10 Mon 13:27]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 13:28
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix requiring (in-package :plong)
   CLOSED: [2017-04-11 Tue 11:59]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 15:10
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: TODO
   :END:

** DONE fix text box only showing one line
   CLOSED: [2017-04-10 Mon 15:21]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 15:21
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE add help key
   CLOSED: [2017-04-10 Mon 15:22]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 15:22
   :ARCHIVE_FILE: ~/xelf/gui.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: gui
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix error when pressing left-alt or other keys on Textbox 
   CLOSED: [2017-04-10 Mon 15:37]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 15:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix help box drawing order
   CLOSED: [2017-04-10 Mon 15:31]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 15:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix notifications drawing z order 
   CLOSED: [2017-04-10 Mon 15:31]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 15:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix C-k not working to kill prompt line
   CLOSED: [2017-04-10 Mon 15:47]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 15:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix cut/copy/paste anomalies with assertions/tests
   CLOSED: [2017-04-10 Mon 19:51]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 19:51
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE recover menubar
   CLOSED: [2017-04-10 Mon 20:02]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 20:02
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] Halo only visible for mouseovered object
   CLOSED: [2017-04-10 Mon 20:29]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 20:29
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] Indicate selection with either halo or subtle outline
   CLOSED: [2017-04-11 Tue 11:59]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 20:29
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** DONE pause/unpause 
   CLOSED: [2017-04-10 Mon 20:29]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-10 Mon 20:30
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] switch-to-buffer
   CLOSED: [2017-04-11 Tue 11:59]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-11 Tue 11:59
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] Dont' create shell/menubar until needed
   CLOSED: [2017-04-11 Tue 11:59]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-11 Tue 11:59
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] Field for enabling mouse ops / M-x in buffer
   CLOSED: [2017-04-11 Tue 10:28]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-11 Tue 11:59
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] test rename-buffer
   CLOSED: [2017-04-11 Tue 11:59]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-11 Tue 11:59
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE remove REVIVE-P arg from ADD-NODE
   CLOSED: [2017-04-11 Tue 18:58]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-11 Tue 18:58
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE split up menus declaration var
   CLOSED: [2017-04-11 Tue 16:19]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-11 Tue 18:58
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] menu halo handle
   CLOSED: [2017-04-11 Tue 18:59]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-11 Tue 18:59
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] fix de-selecting objects when opening menubar
   CLOSED: [2017-04-11 Tue 19:09]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-11 Tue 19:09
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE clean up how node macro VSlots work
   CLOSED: [2017-04-12 Wed 12:22]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-12 Wed 12:22
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:
*** DONE rename to define-visual-macro and with-visual-slots and define nice accessors
    CLOSED: [2017-04-12 Wed 12:22]

** DONE [#A] make z-sorted drawing the default
   CLOSED: [2017-04-12 Wed 16:46]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-12 Wed 16:46
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE change existing struct into class
   CLOSED: [2017-04-15 Sat 20:28]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-15 Sat 20:29
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Write Facade classes for subsystems/Resource class
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** TODO find-resource gets object to user
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-15 Sat 20:29
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Write Facade classes for subsystems/Resource class
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** DONE draw non-working menu items as greyed
   CLOSED: [2017-04-18 Tue 01:18]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-18 Tue 19:32
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE remove old titlebar
   CLOSED: [2017-04-19 Wed 09:58]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE improve layout of properties dialog by proper column alignment
   CLOSED: [2017-04-19 Wed 16:47]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE define-properties-dialog
   CLOSED: [2017-04-19 Wed 14:21]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE custom label entry displays pretty-string but evals to ugly-symbol
   CLOSED: [2017-04-19 Wed 16:42]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix editable titlebar
   CLOSED: [2017-04-19 Wed 16:42]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix clickable non-read-only toggle
   CLOSED: [2017-04-19 Wed 14:37]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE remove package prefix from property sheet names
   CLOSED: [2017-04-19 Wed 14:37]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE destroy dialog when destroying frame
   CLOSED: [2017-04-19 Wed 15:09]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE redesign system menu
   CLOSED: [2017-04-18 Tue 19:34]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:47
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] Fix command prompt not clearing
   CLOSED: [2017-04-19 Wed 16:52]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 16:52
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] fix TAB error
   CLOSED: [2017-04-19 Wed 17:39]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 17:40
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] fix eval not happening
   CLOSED: [2017-04-19 Wed 18:37]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-19 Wed 18:37
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#C] fix BACKTAB not working (is BACKTAB being called?
   CLOSED: [2017-04-20 Thu 06:01]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-20 Thu 06:01
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] fix exploding dialog 
   CLOSED: [2017-04-20 Thu 05:42]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-20 Thu 06:01
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] fix click in text doesn't go to correct column 
   CLOSED: [2017-04-20 Thu 07:30]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-20 Thu 08:38
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#C] fix clicking away from menubar doesn't close menus or allow focusing
   CLOSED: [2017-04-20 Thu 07:30]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-20 Thu 08:38
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] make context-menu on frame go to contents
   CLOSED: [2017-04-20 Thu 08:37]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-20 Thu 08:38
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] plist/variable properties editor
   CLOSED: [2017-04-20 Thu 20:28]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-20 Thu 20:28
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE postpone project creation/loading dialogs, focus on working within one project
   CLOSED: [2017-04-18 Tue 19:34]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-20 Thu 20:29
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#C] Adjust highlight draw in top right corner of rounded patch
   CLOSED: [2017-04-20 Thu 20:14]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-20 Thu 20:31
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] fix command line has no cursor
   CLOSED: [2017-04-21 Fri 07:53]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-21 Fri 08:12
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** TODO switch to menubar and shell not being in buffer
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-21 Fri 08:40
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:
*** DONE fix updating
    CLOSED: [2017-04-21 Fri 08:15]
*** DONE fix drawing
    CLOSED: [2017-04-21 Fri 08:15]
*** DONE fix clicks/focus
    CLOSED: [2017-04-21 Fri 08:39]

** DONE [#A] fix shell prompt not highlighting in switched-to buffer
   CLOSED: [2017-04-21 Fri 13:53]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-21 Fri 13:53
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** TODO [#B] fix menu clicks sometimes not working
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 08:25
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:
*** TODO TAP MENUBAR should forward click to menu

** DONE [#B] fix *untitled-buffer* not being added to list
   CLOSED: [2017-04-22 Sat 07:54]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 12:00
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE fix dialog centering issue
   CLOSED: [2017-04-22 Sat 16:53]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 16:57
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] close after eval for some frames
   CLOSED: [2017-04-22 Sat 16:57]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 16:57
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] basic switch-to-buffer textbox
   CLOSED: [2017-04-22 Sat 16:57]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 16:57
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#C] fix menubar not drawing over dialogs 
   CLOSED: [2017-04-22 Sat 17:30]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:30
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#C] don't allow halo on properties dialog
   CLOSED: [2017-04-22 Sat 17:30]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:30
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** TODO [#A] create buffer switch menu
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:30
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** DONE [#A] show-project-properties-dialog
   CLOSED: [2017-04-22 Sat 17:45]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] edit-cut
   CLOSED: [2017-04-22 Sat 17:44]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] edit-copy
   CLOSED: [2017-04-22 Sat 17:44]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] edit-paste
   CLOSED: [2017-04-22 Sat 17:44]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] show-paste-as-new-buffer-dialog
   CLOSED: [2017-04-22 Sat 17:44]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] transport-play
   CLOSED: [2017-04-22 Sat 17:44]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] transport-pause
   CLOSED: [2017-04-22 Sat 17:45]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] show-create-buffer-dialog
   CLOSED: [2017-04-22 Sat 17:45]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] show-buffer-properties-dialog
   CLOSED: [2017-04-22 Sat 17:45]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] view-buffer-list
   CLOSED: [2017-04-22 Sat 17:45]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 17:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] context-menu with reset-to-current-values
   CLOSED: [2017-04-22 Sat 19:09]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 19:14
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:
*** DONE pin down context menu elements
    CLOSED: [2017-04-22 Sat 18:47]
*** DONE fix broken layout/rendering o context menu
    CLOSED: [2017-04-22 Sat 18:47]

** DONE [#A] fix halos not always being in front
   CLOSED: [2017-04-22 Sat 19:09]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-22 Sat 19:14
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** TODO [#C] ENTER should update value in property field 
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** TODO [#C] add nice Apply/Cancel buttons
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** DONE [#A] show error bubble and restore value when input incorrect
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] Split MAKE-HALO and selection concept
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/fix dialog boxes
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:
*** DONE revise selection functions to use flag instead of HALO slot
    CLOSED: [2017-04-23 Sun 10:41]

** DONE [#A] edit-select-all
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] edit-clear-selection
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] show-copyright-notice
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] show-help
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] edit-invert-selection
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] show-switch-to-buffer-dialog
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] do-trim
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] do-trim-conservatively
   CLOSED: [2017-04-23 Sun 10:41]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:41
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list/implement all main system menu dialogs
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] fix disappearing shell prompt when clicking shell
   CLOSED: [2017-04-23 Sun 10:42]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 10:42
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** TODO quick demo vid
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:02
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

*** TODO show selecting object
*** TODO show select/resize
*** TODO show multi-select
*** TODO show multi-move
*** TODO show select-all
*** TODO show clear-selection
*** TODO show invert-selection
*** TODO show cut, copy, paste
*** TODO show rectangle-select
*** TODO show paste-as-new-buffer
*** TODO show trim empty space
*** TODO show buffer properties
**** TODO show tabbing
**** TODO show background color
**** TODO show error handling of all props before any set
**** TODO show restore initial values
**** TODO show context menu
*** TODO show switch-to-buffer 
*** TODO show project-properties 
*** TODO show help
*** TODO show help context menu

** TODO [#A] fix any move handle should move group
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:08
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** TODO [#A] context-menus
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:08
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** DONE [#B] general properties browser
   CLOSED: [2017-04-23 Sun 11:08]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:08
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:
*** TODO shell create/pop out 1 property of 1 object
*** TODO shell create/pop out property/method browser
*** TODO controls in sidebar?
*** TODO create GUI for scrolling the window

** TODO [#C] change xelf system menu to dark colors
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:08
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** TODO [#C] fix POSITION-WITHIN-PARENT error on pressing TAB
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:09
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** TODO [#C] right click pop up menus
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:09
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** DONE Project class
   CLOSED: [2017-04-23 Sun 11:09]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:09
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:
*** TODO current-project function to get object
*** TODO CREATE
*** TODO OPEN-P
*** TODO CLOSE
*** TODO PLAY
*** TODO PATH
*** TODO SAVE
*** TODO LOAD
*** TODO SAVE-BUFFER
*** TODO LOAD-BUFFER
*** TODO SAVE-SESSION
*** TODO LOAD-SESSION
*** TODO FIND-FILE
*** TODO FILES
*** TODO SOUNDS
*** TODO IMAGES
*** TODO MUSIC
*** TODO BUFFERS
*** TODO EXPORT-APPLICATION
*** TODO EXPORT-ARCHIVE

** TODO Display class for screen properties and drawing
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:09
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** DONE Design how to wrap subsystems in NODE subclasses
   CLOSED: [2017-04-23 Sun 11:10]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:10
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** TODO Change system menu to match new Facades
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:10
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** TODO Write Facade classes for subsystems
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-23 Sun 11:10
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: TODO
   :END:

** DONE [#B] fix data entry widget validation
   CLOSED: [2017-04-24 Mon 11:45]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-24 Mon 11:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#C] solve too-many-halos problem by showing only nearest halo 
   CLOSED: [2017-04-24 Mon 11:46]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-24 Mon 11:46
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] revise/clean up/document new GUI code
   CLOSED: [2017-04-24 Mon 20:18]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-25 Tue 07:13
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] Desktop buffer-class for workspace / task / folder management
   CLOSED: [2017-04-25 Tue 07:13]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-25 Tue 07:13
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:
*** TODO Icon class
*** TODO auto layout simple line rules with spacing
*** TODO auto scale icons and grid with window
*** TODO monochrome icons

** DONE scrolling CellMode buffer
   CLOSED: [2017-04-26 Wed 16:56]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-26 Wed 16:56
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:
*** TODO tree with each row being a list of words
*** TODO passive layout child cell class 
*** TODO review cell-mode code

** DONE Fix FIND-BUFFER registration and too many buffers being created :initarg not respected
   CLOSED: [2017-04-26 Wed 19:00]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-26 Wed 19:00
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE function to determine what row/col was clicked widget
   CLOSED: [2017-04-26 Wed 17:57]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-26 Wed 19:00
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE change buffer list window into cell sheet
   CLOSED: [2017-04-26 Wed 17:11]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-26 Wed 19:00
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** Demo
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-27 Thu 08:45
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_CATEGORY: tasks
   :END:
*** TODO start with desktop
*** TODO show cell sheet
*** TODO show cell selection
*** TODO show layout change in cells when opening/closing menu
*** TODO show create buffer
*** TODO show change class
*** TODO show image browser

** DONE fix ESC binding not being caught by open shell
   CLOSED: [2017-04-28 Fri 07:27]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-28 Fri 07:55
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#B] allow double click desktop icons
   CLOSED: [2017-04-26 Wed 18:39]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-28 Fri 07:55
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:

** DONE [#A] change-buffer-class
   CLOSED: [2017-04-26 Wed 19:30]
   :PROPERTIES:
   :ARCHIVE_TIME: 2017-04-28 Fri 07:55
   :ARCHIVE_FILE: ~/xelf/tasks.org
   :ARCHIVE_OLPATH: Task list
   :ARCHIVE_CATEGORY: tasks
   :ARCHIVE_TODO: DONE
   :END:
