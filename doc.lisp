;;; doc.lisp --- extract Lisp docstrings into org-mode publishing

;; Copyright (C) 2009-2017 by David O'Toole

;; Author: David O'Toole deeteeoh1138@gmail.com
;; Keywords: lisp, tools

;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without restriction,
;; including without limitation the rights to use, copy, modify, merge,
;; publish, distribute, sublicense, and/or sell copies of the Software,
;; and to permit persons to whom the Software is furnished to do so,
;; subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Known issues:

;; Needs to be refactored a bit.
;; Only works on SBCL for now.

;; (defpackage #:dox
;;   (:use #:cl #:xelf)
;;   (:export dox))

;; (in-package :dox)
(in-package :xelf)

(defun get-symbols (package)
  (let (symbols)
    (do-external-symbols (symbol (find-package package))
      (push symbol symbols))
    (sort symbols #'string<)))

(defvar *symbol-count* 0)

(defun methodp (symbol)
  (fboundp symbol))

(defun heading (level text stream)
  (fresh-line stream) 
  (format stream "~A ~A" 
   (make-string level :initial-element (character "*"))
   text)
  (fresh-line stream))

(defun arguments-prefix (stream) 
  (format stream " "))

(defun document-function (symbol stream)
  (let ((doc (documentation symbol 'function))
	(args (sb-introspect:function-lambda-list (or (macro-function symbol)
						      (fdefinition symbol)))))
    (heading 2 (format nil "~A (~A)" symbol (if (macro-function symbol) "macro"
						(if (find-class symbol nil)
						    "class"
						    (if (typep (fdefinition symbol) 'standard-generic-function)
							"generic function"
							"function"))))
	     stream)
    (when (not (null (first args)))
      (arguments-prefix stream)
      (fresh-line stream)
      (fresh-line stream)
      (format stream ": ~S" args))
    (when doc
      (incf *symbol-count*)
      (fresh-line stream)
      (format stream "~A" doc)
      (fresh-line stream))))

(defun document-variable (symbol stream)
  (heading 2 (format nil "~A (variable)" symbol)
	   stream)
  (when (documentation symbol 'variable)
    (incf *symbol-count*)
    (format stream "~A" (documentation symbol 'variable)))
  (fresh-line stream))

(defun document-all-symbols (package stream)
  (fresh-line stream)
  (dolist (sym (get-symbols package))
    (fresh-line)
    (if (fboundp sym)
	(document-function sym stream)
	(document-variable sym stream))))

(defun preamble-file-lines (preamble-file)
  (with-open-file (file preamble-file
			:direction :input
			:if-does-not-exist nil)
    (let* ((len (file-length file))
           (string (make-string len)))
      (read-sequence string file)
      (split-string-on-lines string))))

(defun clean-name (name)
  (substitute #\_ #\+ 
	      (substitute #\_ #\* name))) 

(defun make-symbol-pathname (name directory)
  (make-pathname :name (concatenate 'string (clean-name name) ".org")
		 :defaults (cl-fad:pathname-as-directory directory)))

(defun make-dictionary (package-name directory &optional buffer)
  (let ((package (find-package package-name))
	(symbols nil)
	(*symbol-count* 0))
    (do-external-symbols (symbol package)
      (push symbol symbols))
    (dolist (sym symbols)
      (let ((output-file (make-symbol-pathname (symbol-name sym) directory)))
	(with-open-file (stream output-file :direction :output :if-exists :supersede)
	  (format stream "#+OPTIONS: *:nil")
	  (fresh-line stream)
	  (if (fboundp sym)
	      (document-function sym stream)
	      (document-variable sym stream)))))))
    
(defun make-reference-* (package-name &key (stream t) preamble-file title)
  (let ((package (find-package package-name))
	symbols functions variables
	(*symbol-count* 0))
    ;; header
    (when title 
      (format stream "#+TITLE: ~A" title)
      (fresh-line stream))
    (format stream "#+OPTIONS: toc:2 *:nil")
    (fresh-line stream)
    ;;(format stream "#+INFOJS_OPT: view:info toc:t tdepth:1")
    (fresh-line stream)
    (fresh-line stream)
    (do-external-symbols (symbol package)
      (push symbol symbols))
    ;; print preamble
    (let ((preamble-lines 
	    (when preamble-file
	      (preamble-file-lines preamble-file))))
      (when preamble-file 
	(dolist (line preamble-lines)
	  (format stream "~A " line)
	  (fresh-line stream))))
    (document-all-symbols package-name stream)
    (message "Documented ~S of ~S symbols." *symbol-count* (length symbols))))

(defun make-reference (package-name output-file &key preamble-file title)
  (with-open-file (stream output-file :direction :output :if-exists :supersede)
    (make-reference-* package-name :title title :stream stream :preamble-file preamble-file)))

;; (make-dictionary :xelf #P"~/xelf/doc/dictionary/")
;; (make-reference :xelf #P"~/xelf/doc/reference.org" :preamble-file #P"~/xelf/doc/preamble.org" )

;;; doc.lisp ends here
